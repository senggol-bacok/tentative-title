using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UI.Log
{
    public class CustomText : MonoBehaviour
    {
        public string text;
        public float time;
        LogManager log;
        
        public void SetText(string text, float time, LogManager log)
        {
            this.text = text;
            this.log = log;
            this.time = time;
        }
    }
}

