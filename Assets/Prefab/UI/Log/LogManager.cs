using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Log
{
    public class LogManager : MonoBehaviour

    {
        LinkedList<CustomText> textLL;
        public TMPro.TMP_Text scrollText;
        public static LogManager instance;
        bool progress;

        private void Awake()
        {
            textLL = new LinkedList<CustomText>();
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(gameObject);
                return;
            }

            DontDestroyOnLoad(gameObject);
            scrollText.text = "";
        }

        private void Start()
        {
            progress = false;
        }

        IEnumerator DestroyLastText()
        {
            progress = true;
            while (progress && textLL.First != null)
            {
                yield return new WaitForSeconds(textLL.Last.Value.time);
                Destroy(textLL.Last.Value);
                textLL.RemoveLast();
                printText();
            }
            progress = false;
            
        }

        public void CreateText(string text, float time)
        {
            CustomText temp = gameObject.AddComponent<CustomText>();
            temp.SetText(text, time, this);
            textLL.AddFirst(temp);
            printText();
            
            StartCoroutine(DestroyLastText());
        }

        public void Reset()
        {
            StopAllCoroutines();
            foreach (CustomText text in textLL)
            {
                Destroy(text);
            }
            textLL.Clear();
            printText();
        }

        public void printText()
        {
            string s = "";
            foreach (CustomText text in textLL)
            {
                s += text.text + "\n";
            }
            scrollText.text = s;
        }
    }
}

