using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Cam
{
    public class Base : MonoBehaviour
    {
        public Transform player;
        public float lerpSpeed;
        public bool isRun;

        void FixedUpdate()
        {
            if (isRun)
            {
                Vector3 movement = new Vector3(0, 0, -6);
                movement.x = Mathf.Lerp(transform.position.x, player.position.x, lerpSpeed);
                movement.y = Mathf.Lerp(transform.position.y, player.position.y, lerpSpeed);
                transform.position = movement;
            }
            
        }

        public void SetRun(bool value)
        {
            if (value)
            {
                isRun = true;
            } else
            {
                isRun = false;
            }
        }

        public void MoveCamera(Vector3 target, float speed)
        {
            StartCoroutine(MoveGlobalCamera(target, speed));
        }

        public IEnumerator MoveGlobalCamera(Vector3 pos, float speed)
        {
            Vector3 dist = Vector3.MoveTowards(transform.position, pos, speed * Time.deltaTime);
            while (pos != transform.position)
            {
                transform.position = dist;
                dist = Vector3.MoveTowards(transform.position, pos, speed * Time.deltaTime);
                yield return new WaitForEndOfFrame();
            }
        }

    }

}
