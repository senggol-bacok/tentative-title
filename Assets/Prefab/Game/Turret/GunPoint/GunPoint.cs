using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Turret
{
    public class GunPoint : MonoBehaviour
    {
        public Transform target;

        private void Start()
        {
            GetComponent<SpriteRenderer>().enabled = false;
        }
        public void Shooting(float speed, string type)
        {
            Vector3 globalPos = transform.TransformPoint(Vector2.zero);
            Vector2 dir = globalPos - target.position;
            float angle = Mathf.Atan2(dir.x, dir.y) * Mathf.Rad2Deg;
            if (angle != 0)
            {
                angle *= -1;
            }
            Quaternion q = Quaternion.Euler(0f, 0f, angle);

            GameObject bullet = Pooler.Base.SharedInstance.GetPooledObject(type);
            if (bullet != null)
            {
                bullet.transform.position = transform.position;
                bullet.transform.rotation = q;
                bullet.SetActive(true);
            }
            Bullet.Base bulletScript = bullet.GetComponent<Bullet.Base>();
            bulletScript.setSpeed(speed);
        }
    }
}

