using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Turret
{
    public class Base : MonoBehaviour
    {
        [SerializeField] protected List<GunPoint> gunPointList;
        public Transform gunPointGroups;
        private Coroutine shoot;
        [SerializeField] protected float speed;
        [SerializeField] protected float cd;
        [SerializeField] protected float interval;
        [SerializeField] protected int multiple;
        public TYPE type;
        public enum TYPE {laser, bomb}

        public Animator animator;

        protected virtual void Start()
        {
            if (animator == null)
            {
                animator = GetComponent<Animator>();
            }
            gunPointList = new List<GunPoint>();
            int gunPointCount = gunPointGroups.childCount;
            for (int i = 0; i < gunPointCount; i++)
            {
                GunPoint gunPoint = gunPointGroups.GetChild(i).GetComponent<GunPoint>();
                gunPoint.target = gunPointGroups;
                gunPointList.Add(gunPoint);
            }
        }

        protected IEnumerator Shooting()
        {
            while (true)
            {
                for (int i = 0; i < multiple; i++)
                {
                    foreach (GunPoint gunPoint in gunPointList)
                    {
                        gunPoint.Shooting(speed, type.ToString());

                    }
                    yield return new WaitForSeconds(interval);
                }
                yield return new WaitForSeconds(cd);
            }
        }

        public void FOVReceiver(bool isSee, Collider2D collision)
        {
            if (isSee)
            {
                See(collision);
            }
            else
            {
                UnSee(collision);
            }
        }

        public virtual void See(Collider2D collision)
        {
            if (collision.transform.CompareTag("Player"))
            {
                shoot = StartCoroutine(Shooting());
            }
        }

        public virtual void UnSee(Collider2D collision)
        {
            if (collision.transform.CompareTag("Player"))
            {
                StopCoroutine(shoot);
            }
        }

        private void OnDestroy()
        {
            StopAllCoroutines();
        }

    }

}
