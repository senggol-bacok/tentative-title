using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Turret
{
    public class Targeting : MonoBehaviour
    {
        private Transform target;
        private bool isRotate;
        public float angle;
        public float diff;
        public float rotationSpeed;
        private Coroutine rotate;

        public void FOVReceiver(bool isSee, Collider2D collision)
        {
            if (isSee)
            {
                See(collision);
            }
            else
            {
                UnSee(collision);
            }
        }

        public void See(Collider2D collision)
        {
            if (collision.transform.CompareTag("Player") && !isRotate)
            {
                isRotate = true;
                target = collision.transform;
                rotate = StartCoroutine(Rotate());
            }
        }

        public void UnSee(Collider2D collision)
        {
            if (collision.transform.CompareTag("Player") && isRotate)
            {
                StopCoroutine(rotate);
                isRotate = false;
                target = null;
                
            }
        }

        

        IEnumerator Rotate()
        {
            while (true)
            {
                Vector2 dir = transform.position - target.transform.position;
                angle = -Mathf.Atan2(dir.x, dir.y) * Mathf.Rad2Deg + 180;
                diff = angle - transform.eulerAngles.z;
                if (diff > -1 && diff < 1)
                {
                    ;
                }
                else if (diff > 0 && diff <= 180 || -diff > 180)
                {
                    transform.Rotate(0, 0, 1.0f * Time.deltaTime * rotationSpeed);
                }
                else if (diff > 180 || diff < 0)
                {
                    transform.Rotate(0, 0, -1.0f * Time.deltaTime * rotationSpeed);
                }
                yield return new WaitForFixedUpdate();
            }
        }

    }

}
