using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Turret.Bullet
{
    public class Bomb : Base
    {
        private Animator anim;
        private Rigidbody2D rb2d;

        private void Start()
        {
            anim = GetComponent<Animator>();
            rb2d = GetComponent<Rigidbody2D>();
        }
        public override void setSpeed(float speed)
        {
            GetComponent<Rigidbody2D>().AddForce(transform.up * speed * 1000);
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            explode();
        }

        private void explode()
        {

            Collider2D[] hits = Physics2D.OverlapCircleAll(transform.position, 1f);
            foreach (Collider2D hit in hits)
            {
                Effect.ExplodeOnContact target;
                if (hit.transform.TryGetComponent(out target))
                {
                    target.Explode();
                }
            }
            StartCoroutine(ExplodeAnim());
        }

        IEnumerator ExplodeAnim()
        {
            anim.Play("Explosion");
            rb2d.constraints = RigidbodyConstraints2D.FreezeAll;
            yield return new WaitForSeconds(0.5f);
            rb2d.constraints = RigidbodyConstraints2D.None;
            this.gameObject.SetActive(false);
        }

        private void OnDrawGizmos()
        {
            Gizmos.DrawSphere(transform.position, 1f);
        }
    }
}

