using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Turret.Bullet
{
    public class Base : MonoBehaviour
    {
        [SerializeField] protected float speed;

        public virtual void setSpeed(float speed)
        {
            this.speed = speed;
        }

        
    }
}

