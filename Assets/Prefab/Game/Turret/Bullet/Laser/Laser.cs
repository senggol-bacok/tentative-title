using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.Turret.Bullet
{
    public class Laser : Base
    {

        public override void setSpeed(float speed)
        {
            this.speed = speed;
        }

        private void Update()
        {
            transform.position += transform.up * Time.deltaTime * speed;
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.transform.CompareTag("Player")) {
                string sceneRestartTo = SceneManager.GetActiveScene().name;
                Manager.GameManager.instance.ChanceScene(sceneRestartTo, true);
            }
            if (!
                (collision.transform.CompareTag("Bullet")
                ))
            {
                this.gameObject.SetActive(false);
            }
        }
    }
}

