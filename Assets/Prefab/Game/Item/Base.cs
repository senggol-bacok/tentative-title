using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Item
{
    public abstract class Base : MonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D collision)
        {
            GetItem();
        }

        public abstract void GetItem();
    }
}

