using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Game.Item.GravityGun
{
    public class GravityGun : Base
    {
        public UnityEvent player;
        public override void GetItem()
        {
            player.Invoke();
            Destroy(gameObject);
        }

    }
}

