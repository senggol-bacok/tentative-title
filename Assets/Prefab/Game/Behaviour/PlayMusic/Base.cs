using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Behaviour.PlayMusic
{
    public class Base : MonoBehaviour
    {
        public bool autoplay;
        public string key;
        // Start is called before the first frame update
        void Start()
        {
            if (autoplay)
            {
                Play();
            }
        }


        public void Play()
        {
            if (Manager.Data.instance.dictString["Music"] != key)
            {

                Manager.SoundManager.instance.PlayMusic(key);
                Manager.Data.instance.dictString["Music"] = key;
            }
            

        }
    }

}
