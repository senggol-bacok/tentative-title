using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Behaviour.ChangingSprite
{
    public class Base : MonoBehaviour
    {
        public SpriteRenderer spriteRenderer;
        public Sprite newSprite;

        void Start()
        {
            spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        }

        void ChangeSprite()
        {   
            spriteRenderer.sprite = newSprite;
        }

        public void SignalActive()
        {
            ChangeSprite();
        }
    }
}
