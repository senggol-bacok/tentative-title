using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Behaviour.Patrol
{
    public class Base : MonoBehaviour
    {
        public Vector2 start;
        public Vector2 end;
        public float speed;
        private Vector2 target;
        public bool isForward;
        private Vector2 current;

        void Start()
        {
            if (isForward)
            {
                target = end;
            } else
            {
                target = start;
            }
        }

        // Update is called once per frame
        void Update()
        {
            transform.position = Vector2.MoveTowards(transform.position, target, speed * Time.deltaTime);
            if (Mathf.Abs(Vector2.Distance(transform.position, target)) < 0.5)
            {
                if (isForward)
                {
                    target = start;
                } else
                {
                    target = end;
                }
                isForward = !isForward;
            }
            
        }
    }

}
