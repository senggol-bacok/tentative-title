using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Game.Behaviour.Special
{
    public class Base : MonoBehaviour
    {
        public UnityEvent<int> events;
        public int specialCode;
        public void ReceiveSignal()
        {
            events.AddListener(Manager.GameManager.instance.level.SpecialCase);
            events.Invoke(specialCode);
        }

    }

}

