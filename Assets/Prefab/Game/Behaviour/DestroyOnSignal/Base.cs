using Game.Signal;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Behaviour.DestroyOnSignal
{
    public class Base : MonoBehaviour, Receiver
    {
        public void SignalActive()
        {
            Destroy(gameObject);
        }

        public void SignalDeactive()
        {
            throw new System.NotImplementedException();
        }

    }
}

