using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Behaviour.MovingPlatform
{
    public class Base : MonoBehaviour
    {
        // public Vector2 start;
        public Vector2 end;
        public float speed;
        private Vector2 target;
        public bool isForward;
        private Vector2 current;
        public bool isOnSignal;
        private bool isMoving = true; // Use only when using signals

        void Start()
        {
            target = end;
            if (isOnSignal)
            {
                isMoving = false;
            } else 
            {
                isMoving = true;
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (isMoving)
            {
                transform.position = Vector2.MoveTowards(transform.position, target, speed * Time.deltaTime);
                if (Mathf.Abs(Vector2.Distance(transform.position, target)) < 0.5)
                {
                    speed = 0f;
                }
            }
        }

        public void SignalActive()
        {
            isMoving = true;
        }
    }
}
