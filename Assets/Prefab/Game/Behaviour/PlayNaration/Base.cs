using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Game.Behaviour.PlayNaration
{
    public class Base : MonoBehaviour
    {
        public string key;
        public string text;
        public float time;
        private bool isActive;
        public void Play()
        {
            StartCoroutine(Manager.GameManager.instance.level.PlayNaration(key, text, time));
            isActive = true;
        }

        public virtual void OnDestroy()
        {
            if (isActive)
            {
                try
                {
                    Manager.GameManager.instance.StopNaration();
                }
                catch
                {
                    ;
                }
               
            }
        }
    }
}

