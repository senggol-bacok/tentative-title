using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

namespace Game.Door
{
    public class Base : MonoBehaviour, Signal.Receiver
    {
        public int maxSignal;
        public string nextScene;
        int signal;
        protected bool playerIn;
        protected bool isActive;
        Animator anim;
        void Start()
        {
            anim = GetComponent<Animator>();
            if (signal >= maxSignal)
            {
                AllSignalActive();
            }
        }

        // Update is called once per frame

        private void OnTriggerEnter2D(Collider2D collision)
        {

            if (collision.transform.CompareTag("Player"))
            {
                playerIn = true;
                if (isActive)
                {
                    Manager.GameManager.instance.ChanceScene(nextScene,false);
                }
                
            }
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.transform.CompareTag("Player"))
            {
                playerIn = false;
            }
        }

        public void SignalActive()
        {
            signal++;
            if (signal >= maxSignal)
            {
                AllSignalActive();
            }
        }

        public void SignalDeactive()
        {
            signal--;
            if (isActive)
            {
                anim.SetTrigger("Close");
            }
            isActive = false;
        }

        protected virtual void AllSignalActive()
        {
            anim.SetTrigger("Open");
            isActive = true;
            Manager.SoundManager.instance.PlaySfx("Door");
            if (playerIn)
            {
                Manager.GameManager.instance.ChanceScene(nextScene, false);
            }
        }
    }

}
