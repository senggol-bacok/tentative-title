using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
namespace Game.Door
{
    public class Hidden : Base
    {
        protected override void AllSignalActive()
        {
            isActive = true;
            if (playerIn)
            {
                SceneManager.LoadScene(nextScene);
            }
        }
    }
}

