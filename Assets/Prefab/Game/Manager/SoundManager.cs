using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Manager
{
    public class SoundManager : MonoBehaviour
    {
        public bool mute;
        public static SoundManager instance;
        public List<Sound> musicList;
        public Dictionary<string, Sound> musics;
        public List<Sound> sfxList;
        public Dictionary<string, Sound> sfxs;
        public List<Sound> narationList;
        public Dictionary<string, Sound> narations;
        private void Awake()
        {
            musics = new Dictionary<string, Sound>();
            sfxs = new Dictionary<string, Sound>();
            narations = new Dictionary<string, Sound>();
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(gameObject);
                return;
            }

            DontDestroyOnLoad(gameObject);
            foreach (Sound s in sfxList)
            {
                s.source = gameObject.AddComponent<AudioSource>();
                s.source.clip = s.clip;

                s.source.volume = s.volume;
                s.source.loop = s.isLoop;
                sfxs.Add(s.name, s);
            }
            sfxList.Clear();

            foreach (Sound s in musicList)
            {
                s.source = gameObject.AddComponent<AudioSource>();
                s.source.clip = s.clip;

                s.source.volume = s.volume;
                s.source.loop = s.isLoop;
                musics.Add(s.name, s);

            }
            musicList.Clear();

            foreach (Sound s in narationList)
            {
                s.source = gameObject.AddComponent<AudioSource>();
                s.source.clip = s.clip;

                s.source.volume = s.volume;
                s.source.loop = s.isLoop;
                narations.Add(s.name, s);

            }
            narationList.Clear();
        }

        public void ResetMusic()
        {
            foreach (Sound s in musics.Values)
            {
                s.source.mute = mute;
            }
        }

        public void MuteMusic(string key)
        {
            musics[key].source.mute = true;
        }

        public void ResumeMusic(string key)
        {
            musics[key].source.mute = mute;
        }

        public void ResetAll()
        {
            ResetMusic();
            ResetNaration();
            ResetSfx();
        }
        public void ResetSfx()
        {
            foreach (Sound s in sfxs.Values)
            {
                s.source.mute = mute;
            }
        }

        public void ResetNaration()
        {
            foreach (Sound s in narations.Values)
            {
                s.source.mute = mute;
            }
        }


        public void PlayMusic(string name)
        {
            StopAllMusic();
            Sound s = musics[name];
            s.source.mute = mute;
            s.source.Play();
        }

        public void PlaySfx(string name)
        {
            Sound s = sfxs[name];
            s.source.mute = mute;
            s.source.Play();
        }
        public void PlayNarations(string name, List<string> sounds)
        {
            StopAllNaration(sounds);
            Sound s = narations[name];
            s.source.mute = mute;
            s.source.Play();
        }

        public void StopMusic(string name)
        {
            Sound s = musics[name];
            s.source.Stop();
        }

        public void StopSfx(string name)
        {
            Sound s = sfxs[name];
            s.source.Stop();
        }

        public void StopAllMusic()
        {
            foreach (Sound s in musics.Values)
            {
                s.source.Stop();
            }
        }
        public void StopAllMusic(List<string> names)
        {
            foreach (string s in names)
            {
                musics[name].source.Stop();
            }

        }

        public void StopAllSfx()
        {
            foreach (Sound s in sfxs.Values)
            {
                s.source.Stop();
            }
        }

        public void StopAllSfx(List<string> names)
        {
            foreach (string s in names)
            {
                sfxs[s].source.Stop();
            }
        }

        public void StopAllNaration(List<string> names)
        {
            foreach (string s in names)
            {
                if (narations.TryGetValue(s, out Sound val))
                {
                    val.source.Stop();
                }
            }
        }
    }

}
