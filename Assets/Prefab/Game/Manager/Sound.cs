using UnityEngine.Audio;
using UnityEngine;


namespace Game.Manager
{
    [System.Serializable]
    public class Sound
    {
        public string name;
        public AudioClip clip;

        [Range(0f, 1f)]
        public float volume;

        public bool isLoop;
        [HideInInspector]
        public AudioSource source;
    }

}
