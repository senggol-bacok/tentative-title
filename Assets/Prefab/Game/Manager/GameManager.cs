using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

namespace Game.Manager
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager instance;
        public Level.Base level;
        public bool canRestart;
        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(gameObject);
                return;
            }

            DontDestroyOnLoad(gameObject);
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.R) && canRestart)
            {
                ChanceScene(SceneManager.GetActiveScene().name, true);
            }
            else if (Input.GetKeyDown(KeyCode.P) && canRestart)
            {
                ForceRestart();
            } else if (Input.GetKeyDown(KeyCode.M)) {
                SoundManager.instance.mute = !SoundManager.instance.mute;
                SoundManager.instance.ResetAll();
            }
        }

        public void ChanceScene(string name, bool isDeath)
        {
            StartCoroutine(ChanceSceneCor(name, isDeath));
        }

        public void ForceRestart()
        {
            StopNaration();
            Data.instance.dictBool.Clear();
            Destroy(level.gameObject);
            ChanceScene(SceneManager.GetActiveScene().name, true);
        }

        IEnumerator ChanceSceneCor(string name, bool isDeath)
        {
            if (isDeath)
            {
                Data.instance.IncreaseDeath();
            } else
            {
                StopNaration();
                Destroy(level.gameObject);
                try
                {
                    //Player.Controller player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player.Controller>();
                    Rigidbody2D rb2d = GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody2D>();
                    GameObject.FindGameObjectWithTag("Player").GetComponent<Player.Controller>().Immune();
                    rb2d.constraints = RigidbodyConstraints2D.FreezeAll;
                } catch
                {
                    ;
                }
                
                canRestart = false;
                if (Util.Transition.Base.instance) {
                    yield return StartCoroutine(Util.Transition.Base.instance.FadeToBlack());
                }
                
                Util.MemoryConverter mc = new Util.MemoryConverter();
                mc.SaveString("level", name);
                Util.SaveAndLoad.SaveSystem(mc);
            }
            
            SceneManager.LoadScene(name);
            canRestart = true;
            yield return null;
        }

        public void StopNaration() {
            try
            {
                StartCoroutine(level.StopCor());
                level.StopAllCoroutines();
                UI.Log.LogManager.instance.Reset();
            } catch
            {
                ;
            }
            
        }
    }

}
