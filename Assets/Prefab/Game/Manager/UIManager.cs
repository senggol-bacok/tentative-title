using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

namespace Game.Manager
{
    public class UIManager : MonoBehaviour
    {

        public void PlayGame()
        {
            StartCoroutine(PlayCor());   
        }

        IEnumerator PlayCor()
        {
            Data.instance.Reset();
            yield return StartCoroutine(Util.Transition.Base.instance.FadeToBlack());
            SceneManager.LoadScene("Level 1");
        }

        public void LoadGame()
        {
            StartCoroutine(LoadCor());
        }
        IEnumerator LoadCor()
        {
            string level = Data.instance.dictString["level"];
            yield return StartCoroutine(Util.Transition.Base.instance.FadeToBlack());
            SceneManager.LoadScene(level);
        }

        

        public void MainMenu()
        {
            StartCoroutine(MainMenuCor());
        }

        IEnumerator MainMenuCor()
        {
            if (Util.Transition.Base.instance)
            {
                yield return StartCoroutine(Util.Transition.Base.instance.FadeToBlack());
            }
            Destroy(GameManager.instance.gameObject);
            
            SceneManager.LoadScene("Main Menu");
        }


    }

}
