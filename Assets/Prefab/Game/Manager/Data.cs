using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Manager
{
    public class Data : MonoBehaviour
    {
        public static Data instance;
        public Dictionary<string, bool> dictBool;
        public Dictionary<string, int> dictInt;
        public Dictionary<string, string> dictString;
        void Awake()
        {
            dictBool = new Dictionary<string, bool>();
            dictInt = new Dictionary<string, int>();
            dictString = new Dictionary<string, string>();
            Util.SaveAndLoad.SetFileName("savefile.txt");
            Util.SaveData data = Util.SaveAndLoad.LoadSystem();
            Util.MemoryConverter mc = new Util.MemoryConverter();

            if (data.dictString.TryGetValue("level", out string level))
            {
                dictString.Add("level", level);
            }
            else
            {
                dictString.Add("level", "Level 1");
            }

            if (data.dictInt.TryGetValue("totalDeath", out int death))
            {
                dictInt.Add("totalDeath", death);
            } else
            {
                dictInt.Add("totalDeath", 0);
            }
            dictString.Add("Music", "");
            
            
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(gameObject);
                return;
            }
            DontDestroyOnLoad(gameObject);
        }

        public void IncreaseDeath()
        {
            dictInt["totalDeath"]++;
            Util.MemoryConverter mc = new Util.MemoryConverter();
            mc.SaveInt("totalDeath", dictInt["totalDeath"]);
            Util.SaveAndLoad.SaveSystem(mc);
        }

        public void Reset()
        {
            Util.MemoryConverter mc = new Util.MemoryConverter();
            mc.SaveInt("totalDeath", 0);
            mc.SaveString("level", "Level 1");
            Util.SaveAndLoad.SaveSystem(mc);
            dictBool.Clear();
            dictInt["totalDeath"] = 0;
            dictString["level"] = "Level 1";
        }
    }
}

