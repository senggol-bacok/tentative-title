using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.Effect
{
    public class ExplodeOnContact : MonoBehaviour
    {
        public bool isPlayer = false;
        public void Explode()
        {
            if (isPlayer) {
                string sceneRestartTo = SceneManager.GetActiveScene().name;
                Manager.GameManager.instance.ChanceScene(sceneRestartTo, true);
            } else {
                Destroy(gameObject);
            }
        }
    }

}
