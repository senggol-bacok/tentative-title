using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Game.Area
{
    public class Base : MonoBehaviour
    {
        public UnityEvent events;
        bool dialogue = false;
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (!dialogue && collision.transform.CompareTag("Player"))
            {
                events.Invoke();
                dialogue = true;
            }
        }

    }
}

