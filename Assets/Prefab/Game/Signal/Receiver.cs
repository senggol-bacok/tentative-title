using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Signal
{
    public interface Receiver
    {
        void SignalActive();
        void SignalDeactive();
    }
}

