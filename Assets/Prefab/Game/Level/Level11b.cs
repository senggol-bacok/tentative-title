using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

namespace Game.Level
{
    public class Level11b : Base
    {

        protected override void Start()
        {
            base.Start();

            StartCoroutine(StartCor());
        }

        IEnumerator StartCor()
        {
            yield return StartCoroutine(PlayNaration("11b-1",
                "WHY DIDN�T YOU LISTEN TO ME?! *sigh* Listen, Player. I am here to help you, really. You should trust me.", 8.8f));
            yield return StartCoroutine(PlayNaration("11b-2",
                "Ehh, no. I don�t think it�s a good idea to trust you. I mean, look around.", 7.2f));
            yield return StartCoroutine(PlayNaration("11b-3",
                "Excuse me, who tf are you?", 3.5f));
            yield return StartCoroutine(PlayNaration("11b-4",
                "I�m the second narrator the game has to generate because you couldn�t do your job. AND YOU HAD ONE JOB.", 8.7f));
            yield return StartCoroutine(PlayNaration("11b-5",
                "By the way, Player, that glitchy glitch kills you. Ha! But of course you know already.", 10f));

        }
    }


}
