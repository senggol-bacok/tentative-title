using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Level
{
    public class Finish : MonoBehaviour
    {
        public TMPro.TMP_Text text;
        // Start is called before the first frame update
        private void Start()
        {
            if (Util.Transition.Base.instance)
            {
                StartCoroutine(Util.Transition.Base.instance.FadeFromBlack());
            }
            Util.SaveData data = Util.SaveAndLoad.LoadSystem();
            int deadCount = data.dictInt["totalDeath"];
            if (deadCount == 0)
            {
                text.text = string.Format("ZERO DEATH!!");
            }
            else if (deadCount == 1)
            {
                text.text = string.Format("Congrats you only died once");
            } 
            else
            {
                text.text = string.Format("Congrats you died {0} times", deadCount);
            }

            Manager.Data.instance.Reset();
        }
    }

}
