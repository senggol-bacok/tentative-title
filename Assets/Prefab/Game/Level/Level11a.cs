using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

namespace Game.Level
{
    public class Level11a : Base
    {
        protected override void Start()
        {
            base.Start();

            StartCoroutine(StartCor());
        }

        IEnumerator StartCor()
        {
            yield return StartCoroutine(PlayNaration("11a-1",
                "Oh, hey! Would you look at that, we�re outta the lab! Huh, when did you change your clothes ?", 8.5f));
            yield return StartCoroutine(PlayNaration("11a-2",
                "I�m not sure where we are, but I think your house is just around the corner.", 6.2f));
            yield return StartCoroutine(PlayNaration("11a-3",
                "Uh oh, the exit is blocked. Can you do something to break this wall?", 5.8f));
        }
    }


}
