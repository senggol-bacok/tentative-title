using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

namespace Game.Level
{
    public class Level15a : Base
    {
        private Coroutine cor;
        protected override void Start()
        {
            base.Start();

            StartCoroutine(StartCor());
        }

        IEnumerator StartCor()
        {
            Manager.GameManager.instance.canRestart = false;
            yield return StartCoroutine(PlayNaration("15a-1",
                "You've done a great job. Isnt that your house over there?", 5f));
            yield return StartCoroutine(PlayNaration("15a-2",
                "Come on in, dont be shy.", 2.5f));

        }

        public override void SpecialCase(int cases)
        {
            switch (cases)
            {
                case 1:
                    cor = StartCoroutine(SpecialOne());
                    break;
                case 2:
                    //StopCoroutine(cor);
                    StartCoroutine(SpecialTwo());
                    break;

            }

        }

        IEnumerator SpecialOne()
        {
            Player.Controller player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player.Controller>();
            player.isRun = false;
            StartCoroutine(PlayNaration("15a-3",
                "Its been a long and exhausting day. Someone will pick you up later. Hell be wearing all-black. You can go rest for now.", 9f));
            yield return StartCoroutine(player.MovePlayerRight(new Vector2(19, -2.495023f), 2f));
        }

        IEnumerator SpecialTwo()
        {
            Cam.Base cam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Cam.Base>();
            Player.Controller player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player.Controller>();
            float time = 2f;
            while (time > 0)
            {
                time -= Time.deltaTime;
                yield return new WaitForFixedUpdate();
            }
            run.Invoke(true);
            player.gameObject.SetActive(false);
            time = 3f;
            while (time > 0)
            {
                time -= Time.deltaTime;
                yield return new WaitForFixedUpdate();
            }
            
            StartCoroutine(PlayNaration("15a-4",
                "Good night.", 2.5f));
            cam.isRun = false;
            yield return StartCoroutine(cam.MoveGlobalCamera(new Vector3(19, 5, cam.transform.position.z), 1f));
            Manager.GameManager.instance.ChanceScene("Finish", false);

        }
    }
}
