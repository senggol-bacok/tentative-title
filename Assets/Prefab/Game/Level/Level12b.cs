using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

namespace Game.Level
{
    public class Level12b : Base
    {
        protected override void Start()
        {
            base.Start();

            StartCoroutine(StartCor());
        }

        IEnumerator StartCor()
        {
            yield return StartCoroutine(PlayNaration("12b-1",
                "Uhh, there�s so much leakage. I guess it�s going pretty bad. They�re doing the best they can, tho.", 9f));
            yield return StartCoroutine(PlayNaration("12b-2",
                "So, you�re taking over now? Did I lose my job?", 5f));
            yield return StartCoroutine(PlayNaration("12b-3",
                "I am intentionally ignoring that question.", 3.5f));
        }
    }


}
