using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

namespace Game.Level
{
    public class Level4 : Base
    {
        public GameObject[] barriers;
        protected override void Start()
        {
            base.Start();
            
            StartCoroutine(StartCor());
        }

        public void GetItem()
        {
            StartCoroutine(GravityCor());
        }
        
        IEnumerator StartCor()
        {
            yield return StartCoroutine(PlayNaration("4-1",
                "Look to your left! Is that-- a gun? I think you should take it with you.", 7.3f));
        }

        IEnumerator GravityCor()
        {
            yield return StartCoroutine(PlayNaration("4-2",
                "This is a gravity gun. It can manipulate gravity. Try playing around with it. Be careful not to hurt yourself.", 8f));
        }
    }


}
