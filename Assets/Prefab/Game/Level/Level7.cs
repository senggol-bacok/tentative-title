using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

namespace Game.Level
{
    public class Level7 : Base
    {
        protected override void Start()
        {
            base.Start();
            
            StartCoroutine(StartCor());
        }

        IEnumerator StartCor()
        {
            yield return StartCoroutine(PlayNaration("7-1",
                "There’s acid here, too. Can you build a bridge? No?", 6f));
        }

        

    }


}
