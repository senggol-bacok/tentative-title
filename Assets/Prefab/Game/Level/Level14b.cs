using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

namespace Game.Level
{
    public class Level14b : Base
    {
        public GameObject[] barriers;
        protected override void Start()
        {
            base.Start();
            StartCoroutine(StartCor());
        }

        IEnumerator StartCor()
        {
            yield return StartCoroutine(PlayNaration("14b-1",
                "HOW DARE YOU!!", 2.4f));
            yield return StartCoroutine(PlayNaration("14b-2",
                "Oh, you�re still here? That mute button�s gotta be busted.", 4.2f));
            yield return StartCoroutine(PlayNaration("14b-3",
                "Hey******", 1f));
            yield return StartCoroutine(PlayNaration("14b-4",
                "Now then. This level is going to be painful, but you�re going to power through. Come on, the ending is near.", 8.5f));
            yield return StartCoroutine(PlayNaration("14b-5",
                "GO TO THE RIGHT DOOR*****", 2.28f));
            yield return StartCoroutine(PlayNaration("14b-6",
                "Now, if you want the good ending, then the left is da wae. Your choice now.", 5.5f));
        }
    }


}
