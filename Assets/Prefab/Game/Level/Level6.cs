using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

namespace Game.Level
{
    public class Level6 : Base
    {
        protected override void Start()
        {
            base.Start();
            
            StartCoroutine(StartCor());
        }

        IEnumerator StartCor()
        {
            yield return StartCoroutine(PlayNaration("6-1",
                "*sniff* Do you smell something citrus? It's from that pink goo over there. Don't touch it, it's acidic.", 8.124f));
            yield return StartCoroutine(PlayNaration("6-2",
                "But I wonder where the acid is coming from? I do hope nothing's leaking down in the power room.", 7.2f));
        }

        

    }


}
