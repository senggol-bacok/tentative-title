using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

namespace Game.Level
{
    public class Level13a : Base
    {
        protected override void Start()
        {
            base.Start();
            StartCoroutine(StartCor());
        }

        IEnumerator StartCor()
        {
            yield return StartCoroutine(PlayNaration("13a-1",
                "Remember about the serum? I think the effect is starting to come off. The next room is going to be crazy.", 6f));
        }
    }


}
