using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

namespace Game.Level
{
    public class Level5 : Base
    {
        public GameObject[] barriers;
        bool boxGone;
        protected override void Start()
        {
            base.Start();
            
            StartCoroutine(StartCor());
        }

        public void BoxTimeOut()
        {
            StartCoroutine(BoxCour());
        }

        IEnumerator StartCor()
        {
            yield return StartCoroutine(PlayNaration("5-1",
                "Wonder what they need all those boxes for.", 4f));
        }

        IEnumerator BoxCour()
        {
            if (!boxGone)
            {
                boxGone = true;
                yield return StartCoroutine(PlayNaration("5-2",
               "Gosh that scares me! So the box vaporizes if it's out for too long. It's ok, go dispense another one.", 10f));
            }
            
        }

    }


}
