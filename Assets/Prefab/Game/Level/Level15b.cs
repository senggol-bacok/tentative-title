using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

namespace Game.Level
{
    public class Level15b : Base
    {
        private Coroutine cor;
        protected override void Start()
        {
            narators = new List<string>();
            Manager.GameManager.instance.level = this;
            StartCoroutine(StartCor());
        }

        IEnumerator StartCor()
        {
            Cam.Base cam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Cam.Base>();
            cam.isRun = false;
            if (Util.Transition.Base.instance)
            {
                StartCoroutine(Util.Transition.Base.instance.FadeFromBlack(3f));
            }
            Manager.GameManager.instance.canRestart = false;
            float time = 7f;
            while (time > 0)
            {
                time -= Time.deltaTime;
                yield return new WaitForFixedUpdate();
            }
            yield return StartCoroutine(cam.MoveGlobalCamera(new Vector3(0, 10, cam.transform.position.z), 1.5f));
            Manager.GameManager.instance.ChanceScene("Finish", false);


        }
    }


}
