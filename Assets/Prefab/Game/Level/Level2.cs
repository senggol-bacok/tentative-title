using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

namespace Game.Level
{
    public class Level2 : Base
    {
        public GameObject[] barriers;
        protected override void Start()
        {
            base.Start();
            
            StartCoroutine(StartCor());
        }

        IEnumerator StartCor()
        {
            Manager.GameManager.instance.canRestart = true;
            yield return StartCoroutine(PlayNaration("2-1",
                "Each door may have different ways to open.", 4f));
        }

        

    }


}
