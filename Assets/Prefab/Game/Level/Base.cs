using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
namespace Game.Level
{
    public class Base : MonoBehaviour
    {
        public UnityEvent<bool> run;
        public List<string> narators;

        protected virtual void Awake()
        {
            if (Manager.GameManager.instance.level != null)
            {
                Destroy(gameObject);
            }
            DontDestroyOnLoad(this.gameObject);
        }
        protected virtual void Start()
        {
            narators = new List<string>();
            Manager.GameManager.instance.level = this;
            if (Util.Transition.Base.instance) {
                StartCoroutine(Util.Transition.Base.instance.FadeFromBlack());
            }


        }

        public IEnumerator StopCor()
        {
            Manager.SoundManager.instance.StopAllNaration(narators);
            yield return null;
        }

        public IEnumerator PlayNaration(string key, string text, float time)
        {
            Manager.SoundManager.instance.PlayNarations(key, narators);
            narators.Add(key);
            UI.Log.LogManager.instance.CreateText(text, time);
            yield return new WaitForSeconds(time);
        }

        public virtual void SpecialCase(int cases)
        {
            ;
        }
    }
}
