using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

namespace Game.Level
{
    public class Level10 : Base
    {
        
        public override void SpecialCase(int cases)
        {
            if (cases == 1)
            {
                StartCoroutine(SpecialOne());
            }
        }

        IEnumerator SpecialOne()
        {
            if (! (Manager.Data.instance.dictBool.TryGetValue("level 10 button", out bool val) && val) )
            {
                Manager.Data.instance.dictBool["level 10 button"] = true;
                yield return StartCoroutine(PlayNaration("10-1",
                "SHIT, it's flooding acid! Quick, climb up before it reaches us!", 7f));
                yield return new WaitForSeconds(2f);
                yield return StartCoroutine(PlayNaration("10-2",
                    "Listen to me, there’s gonna be a door on your right. You will go through the door on your right. You only have one chance.", 8f));
            }
            
        }
    }


}
