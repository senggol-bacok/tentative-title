using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

namespace Game.Level
{
    public class Level13b : Base
    {
        public GameObject[] barriers;
        protected override void Start()
        {
            base.Start();
            StartCoroutine(StartCor());
        }

        IEnumerator StartCor()
        {
            yield return StartCoroutine(PlayNaration("13b-1",
                "The anesthetics are coming off. You�ll be in immense pain by the next level. Why�d you hurt your head so bad tho?", 7f));
            yield return StartCoroutine(PlayNaration("13b-2",
                "Huh? What anesthetics? What nonsense are you talking about!?", 5f));
            yield return StartCoroutine(PlayNaration("13b-3",
                "That�s it. I�m turning off your mic.", 3f));
            yield return StartCoroutine(PlayNaration("13b-4",
                "You little mother*** ???", 2.5f));
        }
    }


}
