using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

namespace Game.Level
{
    public class Level9 : Base
    {
        protected override void Start()
        {
            base.Start();
            
            StartCoroutine(StartCor());
        }

        IEnumerator StartCor()
        {
            yield return StartCoroutine(PlayNaration("9-1",
                "There’s so much acid. We gotta get outta here fast. Go fly on that box with your gravity gun.", 7f));
        }


    }


}
