using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

namespace Game.Level
{
    public class Level14a : Base
    {
        protected override void Start()
        {
            base.Start();
            StartCoroutine(StartCor());
        }

        IEnumerator StartCor()
        {
            yield return StartCoroutine(PlayNaration("14a-1",
                "Hey, do you think you can survive until the end?", 4.5f));
            Manager.SoundManager.instance.MuteMusic("Factory");
            yield return StartCoroutine(PlayNaration("14a-2",
                "Hi. If you like this game, don't forget to like, comment, and subscribe. Also, when in doubt, choose the left door!", 12.5f));
            Manager.SoundManager.instance.ResumeMusic("Factory");
            yield return StartCoroutine(PlayNaration("14a-3",
                "Uhh, haha. Did you hear something? Must be a bug. You know what they always say, the door on the right is the right door!", 12f));
        }
    }


}
