using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

namespace Game.Level
{
    public class Level8 : Base
    {
        protected override void Start()
        {
            base.Start();
            
            StartCoroutine(StartCor());
        }

        IEnumerator StartCor()
        {
            yield return StartCoroutine(PlayNaration("8-1",
                "What, you think flying on boxes doesn’t make any sense? Well, blame the serum for it. Don’t worry, it’ll only get worse from here.", 11f));
        }

        

    }


}
