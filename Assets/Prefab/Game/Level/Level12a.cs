using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

namespace Game.Level
{
    public class Level12a : Base
    {
        protected override void Start()
        {
            base.Start();

            StartCoroutine(StartCor());
        }

        IEnumerator StartCor()
        {
            Manager.GameManager.instance.canRestart = false;
            run.Invoke(false);
            Cam.Base cam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Cam.Base>();
            cam.MoveCamera(new Vector3(-8, 1.5f, cam.transform.position.z), 10f);
            yield return StartCoroutine(PlayNaration("12a-1",
                "What now? A turret that smells fear? They�re doing a horrible job at this.", 6f));
            run.Invoke(true);
            Manager.GameManager.instance.canRestart = true;
            yield return StartCoroutine(PlayNaration("12a-2",
                "INCOMING!!", 2.76f));
        }
    }


}
