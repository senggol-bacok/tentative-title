using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

namespace Game.Level
{
    public class Level3 : Base
    {
        public GameObject[] barriers;
        protected override void Start()
        {
            base.Start();
            
            StartCoroutine(StartCor());
        }

        IEnumerator StartCor()
        {
            yield return StartCoroutine(PlayNaration("3-1",
                "Some doors will close again if you're not fast enough.", 4f));
        }

        

    }


}
