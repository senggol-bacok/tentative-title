using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

namespace Game.Level
{
    public class Level1 : Base
    {
        public GameObject[] barriers;
        protected override void Start()
        {
            base.Start();
            StartCoroutine(StartCor());
            
        }

        IEnumerator StartCor()
        {
            Manager.GameManager.instance.canRestart = false;
            yield return StartCoroutine(PlayNaration("1-1", "Welcome to the Lab. You're finally up after they injected that serum in you.", 5f));
            StartCoroutine(PlayNaration("1-2", "The serum is probably harmless. No worries. Let's get you outta here. Keep going through doors until we find the exit.", 8f));
            /*
            Manager.SoundManager.instance.PlayNarations("1-1", narators);
            narators.Add("1-1");
            UI.Log.LogManager.instance.CreateText("Welcome to the Lab.You're finally up after they injected that serum in you.", 5f);
            yield return new WaitForSeconds(5f);
            Manager.SoundManager.instance.PlayNarations("1-2", narators);
            narators.Add("1-2");
            UI.Log.LogManager.instance.CreateText("The serum is probably harmless. No worries. Let's get you outta here. Keep going through doors until we find the exit.", 8f);
            */
            run.Invoke(true);
            //GameObject[] barriers = GameObject.FindGameObjectsWithTag("Barrier");
            foreach(GameObject barrier in barriers)
            {
                Destroy(barrier);
            }
            yield return null;
            
        }

        

    }


}
