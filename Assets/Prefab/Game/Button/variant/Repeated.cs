using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


namespace Game.Button
{
    public class Repeated : Base
    {
        public float interval;
        public UnityEvent deactiveReceivers;
        public override void Pressed()
        {
            base.Pressed();
            StartCoroutine(PressedCoroutine());

        }

        IEnumerator PressedCoroutine()
        {
            yield return new WaitForSeconds(interval);
            isOn = false;
            anim.SetBool("isOn", false);
        }
    }
}

