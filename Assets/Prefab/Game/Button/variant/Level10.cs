using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Game.Button
{
    public class Level10 : Base
    {
        public UnityEvent<int> specialSignal;
        protected override void Start()
        {
            base.Start();
            if (Manager.Data.instance.dictBool.TryGetValue("level 10 button", out bool val) && val)
            {
                isOn = true;
                anim.SetBool("isOn", true);
                Pressed();
            }
        }

        public override void Pressed()
        {
            specialSignal.AddListener(Manager.GameManager.instance.level.SpecialCase);
            specialSignal.Invoke(1);
            base.Pressed();
        }



    }

}
