using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Game.Button
{
    public class Base : MonoBehaviour
    {
        public UnityEvent activeReceivers;
        protected bool isOn;
        bool isReachable;
        protected Animator anim;
        protected virtual void Start()
        {
            anim = GetComponent<Animator>();
            anim.SetBool("isOn", false);
        }

        private void Update()
        {
            if (isReachable && Input.GetKeyDown(KeyCode.E) && !isOn)
            {
                isOn = true;
                anim.SetBool("isOn", true);
                Pressed();
            }
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.transform.CompareTag("Player"))
            {
                isReachable = true;
            }
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.transform.CompareTag("Player"))
            {
                isReachable = false;
            }
        }

        public virtual void Pressed()
        {
            activeReceivers.Invoke();
        }



    }

}
