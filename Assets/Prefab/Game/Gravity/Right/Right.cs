using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Gravity
{
    public class Right : MonoBehaviour
    {
        public float maxCd;
        public float maxRange;
        float range;
        public float speed;
        bool isMove;
        Vector3 direction;

        private void Start()
        {
            Vector3 mouse_pos;
            Vector3 object_pos;
            mouse_pos = Input.mousePosition;
            mouse_pos.z = 5.23f; //The distance between the camera and object
            object_pos = Camera.main.WorldToScreenPoint(GameObject.FindGameObjectWithTag("Player").transform.position);

            direction = mouse_pos - object_pos;
            direction.z = 0;
            direction.Normalize();

            StartCoroutine(timeUp());
            isMove = true;
        }

        void Update()
        {
            if (Input.GetKey(KeyCode.Mouse1) && isMove && range < maxRange)
            {
                transform.position += direction * speed * Time.deltaTime;
                range += 1 * speed * Time.deltaTime;

            } else if (Input.GetKeyUp(KeyCode.Mouse1))
            {
                isMove = false;
            }
        }

        IEnumerator timeUp()
        {
            yield return new WaitForSeconds(maxCd);
            Destroy(gameObject);
        }
    }
}

