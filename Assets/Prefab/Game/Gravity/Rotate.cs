using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Gravity
{
    public class Rotate : MonoBehaviour
    {
        // Start is called before the first frame update
        public float speed;
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            Vector3 temp = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z + speed);
            transform.eulerAngles = temp;
        }
    }
}

