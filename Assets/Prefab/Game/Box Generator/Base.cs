using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.BoxGenerator
{
    public class Base : MonoBehaviour, Signal.Receiver
    {
        Box.Base box;
        public float timer;
        bool isTimer;
        public GameObject objBox;
        public void SignalActive()
        {
            if (box != null)
            {
                Destroy(box.gameObject);
            }
            GameObject tempObj = Instantiate(objBox, transform.position, Quaternion.identity);
            box = tempObj.GetComponent<Box.Base>();
            if (box is Box.Timer)
            {
                Box.Timer temp = (Box.Timer)box;
                StartCoroutine(temp.TimerRun(timer));
            }
        }

        public void SignalDeactive()
        {
            box.StopAllCoroutines();
            Destroy(box.gameObject);
        }

    }
}

