using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.AcidRising
{

   public class Base : MonoBehaviour
    {
        private float riseHeight;
        public float fastRise;
        public float normalRise;
        public string sceneRestartTo = "";
        public bool isActive = true;
        private Transform acidTransform;
        public GameObject player;
        public Collider2D c2d;
        // Start is called before the first frame update
        void Start()
        {
            acidTransform = this.transform;
            if (sceneRestartTo == "")
            {
                sceneRestartTo = SceneManager.GetActiveScene().name;
            }

            c2d = GetComponent<Collider2D>();
            riseHeight = normalRise;
            
        }

        // Update is called once per frame
        void Update()
        {
            if (isActive)
            {
                if(player.transform.position.y - c2d.bounds.max.y > 6f) {
                    riseHeight = fastRise;
                } else
                {
                    riseHeight = normalRise;
                }
                acidTransform.localScale += new Vector3(0,riseHeight,0);
                acidTransform.position += new Vector3(0, riseHeight, 0);
            }
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.transform.CompareTag("Player"))
            {
                Manager.GameManager.instance.ChanceScene(sceneRestartTo, true);
            }
        }
        
        public void acidActive()
        {
            isActive = true;
        }

        public void acidDeactive()
        {
            isActive = false;
        }

    }

}