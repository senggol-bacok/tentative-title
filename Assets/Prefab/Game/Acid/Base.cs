using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.Acid
{
    public class Base : MonoBehaviour
    {
        public string sceneRestartTo = "";
        public bool isActive = true;
        // Start is called before the first frame update
        void Start()
        {
            if (sceneRestartTo == "")
            {
                sceneRestartTo = SceneManager.GetActiveScene().name;
            }
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.transform.CompareTag("Player") && isActive)
            {
                Manager.GameManager.instance.ChanceScene(sceneRestartTo, true);
            }
        }
        
        public void acidActive()
        {
            isActive = true;
        }

        public void acidDeactive()
        {
            isActive = false;
        }

    }
}
