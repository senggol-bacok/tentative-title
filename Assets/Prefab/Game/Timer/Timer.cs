using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using UnityEngine.UI;
namespace Game.Timer 
{
    public class Timer : MonoBehaviour, Signal.Receiver
    {
        public float time = 5f;
        Coroutine lastRoutine;
        public UnityEvent activeReceivers;
        public UnityEvent deactiveReceivers;
        public TMPro.TMP_Text text;

        private void Start()
        {
            text.text = "";
        }

        IEnumerator Countdown()
        {
            yield return StartCoroutine(TimerRun(time));
            SignalDeactive();
        }

        public void SignalActive()
        {
            activeReceivers.Invoke();
            if (lastRoutine != null)
            {
                StopCoroutine(lastRoutine);
            }
            lastRoutine = StartCoroutine(Countdown());
        }

        IEnumerator TimerRun(float timer)
        {
            float temp = timer;
            while (temp > 0)
            {
                temp -= Time.fixedDeltaTime;
                text.text = string.Format("{0}",Mathf.Ceil(temp));
                yield return new WaitForFixedUpdate();
            }
            text.text = "";
        }

        public void SignalDeactive()
        {
            deactiveReceivers.Invoke();
            Debug.Log("Signal Inactive");
        }
    }
}