using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Game.Player
{

    public class GroundCheck : MonoBehaviour
    {
        public bool onGround = false;

        private void OnTriggerStay2D(Collider2D collision)
        {
            if (collision.transform.CompareTag("Box") || collision.transform.CompareTag("Ground"))
            {
                onGround = true;
            }
            
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.transform.CompareTag("Box") || collision.transform.CompareTag("Ground"))
            {
                onGround = false;
            }
        }
    }
}
