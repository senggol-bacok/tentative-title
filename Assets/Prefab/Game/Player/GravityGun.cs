using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Game.Player
{
    public class GravityGun : MonoBehaviour
    {
        public GameObject point;
        public GameObject leftGravityObj;
        public GameObject rightGravityObj;
        public bool isActive;
        GameObject leftGravity;
        [SerializeField] GameObject rightGravity;
        bool isRight;
        public SpriteRenderer playerSprite;
        // Start is called before the first frame update
        void Start()
        {
            isRight = true;
        }

        // Update is called once per frame
        void Update()
        {
            FollowMouseRotation();
            if (isActive)
            {
                if (Input.GetKeyDown(KeyCode.Mouse0) && leftGravity == null)
                {
                    leftGravity = Instantiate(leftGravityObj, point.transform);
                }
                else if (Input.GetKeyUp(KeyCode.Mouse0) && leftGravity != null)
                {
                    Destroy(leftGravity);
                }
                else if (Input.GetKeyDown(KeyCode.Mouse1) && rightGravity == null)
                {
                    rightGravity = Instantiate(rightGravityObj, point.transform.position, Quaternion.identity, null);
                }
                else if (Input.GetKeyDown(KeyCode.Mouse1) && rightGravity != null)
                {
                    Destroy(rightGravity);
                }
            }
            
            
        }
        void FollowMouseRotation()
        {

            
            Vector3 mouse_pos;
            bool isChanged = false;
            Vector3 object_pos;
            float angle;
            mouse_pos = Input.mousePosition;
            mouse_pos.z = 5.23f; //The distance between the camera and object
            object_pos = Camera.main.WorldToScreenPoint(transform.position);
            mouse_pos.x = mouse_pos.x - object_pos.x;
            mouse_pos.y = mouse_pos.y - object_pos.y;
            angle = Mathf.Atan2(mouse_pos.y, mouse_pos.x) * Mathf.Rad2Deg;
            if (Mathf.Abs(angle) >= 0 && Mathf.Abs(angle) <= 90 && !isRight)
            {
                isRight = true;
                isChanged = true;
            }
            else if (Mathf.Abs(angle) > 90 && Mathf.Abs(angle) <= 180 && isRight)
            {
                isRight = false;
                isChanged = true;
            }
            if (isChanged)
            {
                playerSprite.flipX = !isRight;
            }
            transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));

        }
    }
}

