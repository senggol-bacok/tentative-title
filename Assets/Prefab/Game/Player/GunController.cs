using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Player
{
    public class GunController : MonoBehaviour
    {
        public GravityGun gravityGun;
        public SpriteRenderer sprite;

        public void GunEnabled()
        {
            gravityGun.isActive = true;
            sprite.enabled = true;
        }

        public void GunDisabled()
        {
            gravityGun.isActive = false;
            sprite.enabled = false;
        }
    }

}
