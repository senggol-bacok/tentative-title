using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.Player
{

    public class Controller : MonoBehaviour
    {
        // Start is called before the first frame update
        Rigidbody2D rb2d;
        GroundCheck groundCheck;
        public float jumpPower;
        public float speed;
        Animator anim;
        float movement;
        bool jump;
        public bool isRun;
        void Start()
        {
            rb2d = GetComponent<Rigidbody2D>();
            groundCheck = GetComponentInChildren<GroundCheck>();
            anim = GetComponent<Animator>();

        }
        private void Update()
        {
            if (isRun)
            {
                Move();
                Jump();
            }
        }

        private void FixedUpdate()
        {
            float moveBy = movement * speed;
            rb2d.velocity = new Vector2(moveBy, rb2d.velocity.y);
            anim.SetFloat("speed", Mathf.Abs(rb2d.velocity.x));
            if (jump)
            {
                jump = false;
                rb2d.velocity = new Vector2(rb2d.velocity.x, jumpPower);
            }
            if (rb2d.velocity.y < -20)
            {
                rb2d.velocity = new Vector2(rb2d.velocity.x, -20);
            }

        }
        private void Move()
        {
            movement = 0;
            if (Input.GetKey(KeyCode.A))
            {
                movement = -1;
            }
            if (Input.GetKey(KeyCode.D))
            {
                movement = 1;
            }
        }

        public void SetRun(bool value)
        {
            if (value)
            {
                isRun = true;
            }
            else
            {
                isRun = false;
            }
        }
        private void Jump()
        {
            if (Input.GetKeyDown(KeyCode.Space) && groundCheck.onGround)
            {
                jump = true;

            }
        }

        public void Immune()
        {
            this.tag = "PlayerImmune";
        }

        public IEnumerator MovePlayerRight(Vector2 pos, float speed)
        {
            
            while (pos.x != transform.position.x && pos.x > transform.position.x)
            {
                rb2d.MovePosition((Vector2) transform.position + new Vector2(speed, 0) * Time.deltaTime);
                yield return new WaitForFixedUpdate();
            }
            rb2d.constraints = RigidbodyConstraints2D.FreezeAll;
            rb2d.velocity = Vector2.zero;
            anim.Play("Idle");
        }


    }

}