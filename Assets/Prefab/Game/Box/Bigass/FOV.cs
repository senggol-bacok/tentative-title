using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Game.Box.Bigass
{
    public class FOV : MonoBehaviour
    {

        public UnityEvent receiver;

        private void OnTriggerEnter2D(Collider2D collision)
        {
            receiver.Invoke();
        }
    }
}

