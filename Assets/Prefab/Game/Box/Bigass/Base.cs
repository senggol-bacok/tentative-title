using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.Box.Bigass
{
    public class Base : MonoBehaviour
    {
        private Rigidbody2D rb2d;
        private void Start()
        {
            rb2d = GetComponent<Rigidbody2D>();
            rb2d.constraints = RigidbodyConstraints2D.FreezeAll;
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.transform.CompareTag("Player"))
            {
                string sceneRestartTo = SceneManager.GetActiveScene().name;
                Manager.GameManager.instance.ChanceScene(sceneRestartTo, true);
            }
        }

        public void GroundPound()
        {
            rb2d.constraints = RigidbodyConstraints2D.None;
            rb2d.AddForce(Vector2.down * 100);
        }
    }

}
