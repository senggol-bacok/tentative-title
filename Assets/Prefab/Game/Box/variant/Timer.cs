using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Box
{
    public class Timer : Base
    {
        Animator anim;
        public float temp;
        // Start is called before the first frame update
        void Start()
        {
            anim = GetComponent<Animator>();
        }

        public IEnumerator TimerRun(float timer)
        {
            //float temp = timer;
            temp = timer;
            while (temp > 0)
            {
                if (temp < 5)
                {
                    anim.SetBool("blink", true);
                }
                temp -= Time.fixedDeltaTime;
                yield return new WaitForFixedUpdate();
            }
        }
    }

}
