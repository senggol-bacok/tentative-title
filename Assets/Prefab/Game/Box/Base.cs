using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Box
{
    public class Base : MonoBehaviour
    {

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.transform.CompareTag("Ground"))
            {
                Manager.SoundManager.instance.PlaySfx("Box");
            }
        }
    }
}

