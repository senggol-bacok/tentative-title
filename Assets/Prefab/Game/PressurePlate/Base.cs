using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

namespace Game.PressurePlate
{
    public class Base : MonoBehaviour
    {
        public UnityEvent activeReceivers;
        public UnityEvent deactiveReceivers;
        int pressure;
        Animator anim;
        void Start()
        {
            anim = GetComponent<Animator>();
        }
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.transform.CompareTag("Player") || collision.transform.CompareTag("Box"))
            {
                if (pressure <= 0)
                {
                    anim.SetBool("isOn", true);
                    Pressed();
                }
                pressure++;
            }
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.transform.CompareTag("Player") || collision.transform.CompareTag("Box"))
            {
                pressure--;
                if (pressure < 1)
                {
                    anim.SetBool("isOn", false);
                    UnPressed();
                    
                }
            }
        }

        public void Pressed()
        {
            activeReceivers.Invoke();
        }

        public void UnPressed()
        {
            deactiveReceivers.Invoke();
        }
    }
}

