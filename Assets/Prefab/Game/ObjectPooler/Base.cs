using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Pooler
{
    public class Base : MonoBehaviour
    {
        public static Base SharedInstance;
        public List<GameObject> pooledLasers;
        public GameObject laserToPool;
        public List<GameObject> pooledBombs;
        public GameObject bombToPool;
        public int amountToPool;

        void Awake()
        {
            SharedInstance = this;
        }

        private void Start()
        {
            pooledLasers = new List<GameObject>();
            for (int i = 0; i < amountToPool; i++)
            {
                GameObject obj = (GameObject)Instantiate(laserToPool);
                obj.SetActive(false);
                pooledLasers.Add(obj);
            }
            pooledBombs = new List<GameObject>();
            for (int i = 0; i < amountToPool; i++)
            {
                GameObject obj = (GameObject)Instantiate(bombToPool);
                obj.SetActive(false);
                pooledBombs.Add(obj);
            }
        }

        public GameObject GetPooledObject(string type)
        {
            if (type == "laser")
            {
                return GetPooledLaser();
            } else
            {
                return GetPooledBomb();
            }
        }

        public GameObject GetPooledLaser()
        {
            //1
            for (int i = 0; i < pooledLasers.Count; i++)
            {
                //2
                if (!pooledLasers[i].activeInHierarchy)
                {
                    return pooledLasers[i];
                }
            }
            //3   
            GameObject obj = (GameObject)Instantiate(laserToPool);
            obj.SetActive(false);
            pooledLasers.Add(obj);
            return obj;
        }
        public GameObject GetPooledBomb()
        {
            //1
            for (int i = 0; i < pooledBombs.Count; i++)
            {
                //2
                if (!pooledBombs[i].activeInHierarchy)
                {
                    return pooledBombs[i];
                }
            }
            //3   
            GameObject obj = (GameObject)Instantiate(laserToPool);
            obj.SetActive(false);
            pooledBombs.Add(obj);
            return obj;
        }


    }
}

