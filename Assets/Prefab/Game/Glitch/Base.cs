using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.Glitch
{
    public class Base : MonoBehaviour
    {
        // Start is called before the first frame update
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.transform.CompareTag("Player"))
            {
                Manager.GameManager.instance.ChanceScene(SceneManager.GetActiveScene().name, true);
            }
        }
    }
}

