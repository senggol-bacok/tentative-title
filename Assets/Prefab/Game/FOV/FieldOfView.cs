using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Game.FOV
{
    public class FieldOfView : MonoBehaviour
    {
        public UnityEvent<bool, Collider2D> receiver;

        private void OnTriggerEnter2D(Collider2D collision)
        {
            receiver.Invoke(true, collision);
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            receiver.Invoke(false, collision);
        }

    }

}
