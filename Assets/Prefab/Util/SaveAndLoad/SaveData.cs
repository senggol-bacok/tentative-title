using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Util {
    [System.Serializable]
    public class SaveData
    {
        public Dictionary<string, string> dictString = new Dictionary<string, string>();
        public Dictionary<string, int> dictInt = new Dictionary<string, int>();
        public Dictionary<string, List<int>> dictListInt = new Dictionary<string, List<int>>();
        public Dictionary<string, List<string>> dictListString = new Dictionary<string, List<string>>();

        // Update is called once per frame
        public void SaveMemoryConverter(MemoryConverter memoryConverter)
        {

            DictStringExtractor(memoryConverter.GetDictString());
            DictIntExtractor(memoryConverter.GetDictInt());
            DictListStringExtractor(memoryConverter.GetDictListString());
            DictListIntExtractor(memoryConverter.GetDictListInt());
        }

        public void DictStringExtractor(Dictionary<string, string> dictionaryString)
        {
            foreach (KeyValuePair<string, string> item in dictionaryString)
            {
                dictString[item.Key] = item.Value;
            }
        }

        public void DictIntExtractor(Dictionary<string, int> dictionaryInt)
        {
            foreach (KeyValuePair<string, int> item in dictionaryInt)
            {
                dictInt[item.Key] = item.Value;
            }
        }

        public void DictListIntExtractor(Dictionary<string, List<int>> dictionaryListInt)
        {
            foreach (KeyValuePair<string, List<int>> item in dictionaryListInt)
            {
                dictListInt[item.Key] = item.Value;
            }
        }


        public void DictListStringExtractor(Dictionary<string, List<string>> dictionaryString)
        {
            foreach (KeyValuePair<string, List<string>> item in dictionaryString)
            {
                dictListString[item.Key] = item.Value;
            }
        }


    }

}

