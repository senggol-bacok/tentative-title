using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Util
{
    public static class SaveAndLoad
    {
        private static string fileName;

        public static void SetFileName(string fileNameParam)
        {
            fileName = "/" + fileNameParam;
        }

        public static void CreateFile()
        {
            string path = Application.persistentDataPath + fileName;
            if (File.Exists(path))
            {
                Debug.Log("File already created");
                Debug.Log(path);
            }
            else
            {
                Debug.Log(path);
                BinaryFormatter formatter = new BinaryFormatter();
                path = Application.persistentDataPath + fileName;
                FileStream stream = new FileStream(path, FileMode.Create);
                SaveData data = new SaveData();
                formatter.Serialize(stream, data);
                stream.Close();
            }
        }
        public static void SaveSystem(MemoryConverter memoryConverter)
        {
            string path = Application.persistentDataPath + fileName;
            if (!File.Exists(path))
            {
                CreateFile();
            }
            BinaryFormatter formatter = new BinaryFormatter();
            SaveData data = LoadSystem();
            FileStream stream = new FileStream(path, FileMode.Create);
            data.SaveMemoryConverter(memoryConverter);
            formatter.Serialize(stream, data);
            stream.Close();
        }

        public static SaveData LoadSystem()
        {
            string path = Application.persistentDataPath + fileName;
            if (!File.Exists(path))
            {
                CreateFile();
            }
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);
            SaveData data = formatter.Deserialize(stream) as SaveData;
            stream.Close();
            return data;
        }
    }

}
