using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Util
{
    public class MemoryConverter
    {
        private Dictionary<string, string> dictString = new Dictionary<string, string>();
        private Dictionary<string, int> dictInt = new Dictionary<string, int>();
        private Dictionary<string, List<int>> dictListInt = new Dictionary<string, List<int>>();
        private Dictionary<string, List<string>> dictListString = new Dictionary<string, List<string>>();
        // Update is called once per frame
        public void SaveString(string key, string value)
        {
            dictString.Add(key, value);
        }

        public void SaveInt(string key, int value)
        {
            dictInt.Add(key, value);
        }

        public void SaveListInt(string key, List<int> value)
        {
            dictListInt.Add(key, value);
        }

        public void SaveListString(string key, List<string> value)
        {
            dictListString.Add(key, value);
        }

        public Dictionary<string, string> GetDictString()
        {
            return dictString;
        }

        public Dictionary<string, int> GetDictInt()
        {
            return dictInt;
        }

        public Dictionary<string, List<int>> GetDictListInt()
        {
            return dictListInt;
        }

        public Dictionary<string, List<string>> GetDictListString()
        {
            return dictListString;
        }

    }

}
