using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Util.Transition
{
    public class Base : MonoBehaviour
    {
        public Image blackScreen;
        private bool isFadeIn;
        private bool isFadeOut;
        public float time;
        public static Base instance;

        private void Awake()
        {

            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(gameObject);
                return;
            }

            DontDestroyOnLoad(gameObject);

        }
        public IEnumerator FadeToBlack()
        {

            if (!isFadeOut)
            {
                isFadeOut = true;
                blackScreen.color = Color.black;
                blackScreen.canvasRenderer.SetAlpha(0.0f);
                blackScreen.CrossFadeAlpha(1.0f, time, false);
                yield return new WaitForSeconds(time);
                isFadeOut = false;
            }
            else
            {
                yield return new WaitForSeconds(time);
            }

        }

        public IEnumerator FadeFromBlack()
        {
            if (!isFadeIn)
            {
                isFadeIn = true;
                blackScreen.color = Color.black;
                blackScreen.canvasRenderer.SetAlpha(1.0f);
                blackScreen.CrossFadeAlpha(0.0f, time, false);
                yield return new WaitForSeconds(time);
                isFadeIn = false;
            }
            else
            {
                yield return new WaitForSeconds(time);
            }

        }

        public IEnumerator FadeToBlack(float time)
        {

            if (!isFadeOut)
            {
                isFadeOut = true;
                blackScreen.color = Color.black;
                blackScreen.canvasRenderer.SetAlpha(0.0f);
                blackScreen.CrossFadeAlpha(1.0f, time, false);
                yield return new WaitForSeconds(time);
                isFadeOut = false;
            }
            else
            {
                yield return new WaitForSeconds(time);
            }

        }

        public IEnumerator FadeFromBlack(float time)
        {
            if (!isFadeIn)
            {
                isFadeIn = true;
                blackScreen.color = Color.black;
                blackScreen.canvasRenderer.SetAlpha(1.0f);
                blackScreen.CrossFadeAlpha(0.0f, time, false);
                yield return new WaitForSeconds(time);
                isFadeIn = false;
            }
            else
            {
                yield return new WaitForSeconds(time);
            }

        }
    }

}
